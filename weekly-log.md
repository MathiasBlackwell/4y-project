
# Week of 25th September

- Researched possible game engines to make the game in.
- Downloaded, and check a few of them out.

# Week of 2nd October

- Tested out Godot in detail.
- Followed a tutorial to make a simple 2D game.

# Week of 9th October

- Continued some Godot
- Decided to not use it.
- I will build it up from ground up with Javascript.

# Week of 16th October

- Made a rough prototype in Javascript
- Rethinking my choices.

# Week of 23rd October

- I looked a more tutorials on Godot, to maybe try again.

# Week of 6th November

- No progress..

# Week of 13th November

- Contining in Javascript.
- Made a moving player, and a moving enemy. 
- Made bullets that move, to hit player.

# Week of 20th Novemeber

- Made presentation for the game
- Made player shoot back, and all potential enemies and levels.
- A debug to change levels

# Week of 27th November

- Made small tweaks, with easier level changing for debug mode.

# Week of 4th December

- Made plans of what to do, what I want to achieve.
- Made rough drawings on paper of my player sprite.

# Week of 11th December

- Started working on newer sprites, they will be animated, and a lot bigger.

# Weeks 18th December to 8th January

- No coding progress was done, except a few minor tweaks, and more sprites sketched.
- Decided which level I will make in detail, and have others on backburner.

# Week of 15th January

- Drew rough versions of player and enemy sprites.

# Week of 22nd January

- Made a website to properly host the game, and give it info (Template)

# Week of 29th January

- No progress sadly, overwhelmed by other things.

# Week of 5th February

- Made and implemented sketches of the starter story

# Week of 12th February

- Drew proper sprites of player, and implemented them into the game
- Researched ways to make the whole bullet aspect of the game, and how to make them

# Week of 19th February

- Implemented one enemy sprite into the game.

# Week of 26th February

- Title screen animation: testing different ways to load it, and animate it

# Week of 4th March

- Got the title screen-seqeuence animation figured out: Spritesheet.

# Week of 11th March

- Mental health week
- Different enemy movement.
- Planned different player sprite and overall assets improvement.

# Week of 18th March

- Improving art assets, and taking a small break from code.

# Week of 25th March

- Implemented proper enemy spawning (up to 10 per level! but for level 1 its only 3 at a time).
- Enemy shooting back at player.

# Week of 1st April

- Mental health week/personal week.

# Week of 8th April

- Proper background and instructions
- Better bullets from player.

# Week of 15th April

- Improving assets
- Cleaning up some of the code
- Making website for showcase.