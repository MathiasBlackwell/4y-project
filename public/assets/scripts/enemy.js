//All enemy based functions are here, such as movement, animations etc.
console.log("Enemy Loaded");


function enemy1Move()
{
  e1y += enemySpeed;

  if (e1x <= (playerBullet1X + 50) && (e1x + 75) >= playerBullet1X  && e1y <= (playerBullet1Y + 50) && (e1y + 100) >= playerBullet1Y)
  {
    e1x = Math.floor(Math.random() * 200) + 600;
    e1y = -150;
    playerBullet1Y = -50;
    bullet1shot = false;
    bullet1wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e1x <= (playerBullet2X + 50) && (e1x + 75) >= playerBullet2X  && e1y <= (playerBullet2Y + 50) && (e1y + 100) >= playerBullet2Y)
  {
    e1x = Math.floor(Math.random() * 200) + 600;
    e1y = -150;
    playerBullet2Y = -50;
    bullet2shot = false;
    bullet2wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e1x <= (playerBullet3X + 50) && (e1x + 75) >= playerBullet3X  && e1y <= (playerBullet3Y + 50) && (e1y + 100) >= playerBullet3Y)
  {
    e1x = Math.floor(Math.random() * 200) + 600;
    e1y = -150;
    playerBullet3Y = -50;
    bullet3shot = false;
    bullet3wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }

  if (e1y >= 1500)
  {
    e1x = Math.floor(Math.random() * 200) + 600;
    e1y = -150;
  }
}
function enemy2Move()
{
  e2y += enemySpeed;

  if (e2x <= (playerBullet1X + 50) && (e2x + 75) >= playerBullet1X  && e2y <= (playerBullet1Y + 50) && (e2y + 100) >= playerBullet1Y)
  {
    e2x =  Math.floor(Math.random() * 200) + 400;
    e2y = -50;
    playerBullet1X = -50;
    bullet1shot = false;
    bullet1wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e2x <= (playerBullet2X + 50) && (e2x + 75) >= playerBullet2X  && e2y <= (playerBullet2Y + 50) && (e2y + 100) >= playerBullet2Y)
  {
    e2x =  Math.floor(Math.random() * 200) + 400;
    e2y = -50;
    playerBullet2X = -50;
    bullet2shot = false;
    bullet2wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e2x <= (playerBullet3X + 50) && (e2x + 75) >= playerBullet3X  && e2y <= (playerBullet3Y + 50) && (e2y + 100) >= playerBullet3Y)
  {
    e2x =  Math.floor(Math.random() * 200) + 400;
    e2y = -50;
    playerBullet3X = -50;
    bullet3shot = false;
    bullet3wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }

  if (e2y >= 1500)
  {
    e2x = Math.floor(Math.random() * 200) + 400;
    e2y = -50;
  }
}
function enemy3Move()
{
  e3y += enemySpeed;

  if (e3x <= (playerBullet1X + 50) && (e3x + 75) >= playerBullet1X  && e3y <= (playerBullet1Y + 50) && (e3y + 100) >= playerBullet1Y)
  {
    e3x = Math.floor(Math.random() * 200) + 100;
    e3y = -300;
    playerBullet1X = -50;
    bullet1shot = false;
    bullet1wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e3x <= (playerBullet2X + 50) && (e3x + 75) >= playerBullet2X  && e3y <= (playerBullet2Y + 50) && (e3y + 100) >= playerBullet2Y)
  {
    e3x = Math.floor(Math.random() * 200) + 100;
    e3y = -300;
    playerBullet2X = -50;
    bullet2shot = false;
    bullet2wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e3x <= (playerBullet3X + 50) && (e3x + 75) >= playerBullet3X  && e3y <= (playerBullet3Y + 50) && (e3y + 100) >= playerBullet3Y)
  {
    e3x = Math.floor(Math.random() * 200) + 100;
    e3y = -300;
    playerBullet3X = -50;
    bullet3shot = false;
    bullet3wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }

  if (e3y >= 1500)
  {
    e3x = Math.floor(Math.random() * 200) + 100;
    e3y = -300;
  }

}
function enemy4Move()
{
  e4y += enemySpeed;

  if (e4x <= (playerBullet1X + 50) && (e4x + 75) >= playerBullet1X  && e4y <= (playerBullet1Y + 50) && (e4y + 100) >= playerBullet1Y)
  {
    e4x = Math.floor(Math.random() * 100) + 400;
    e4y = -250;
    playerBullet1X = -50;
    bullet1shot = false;
    bullet1wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e4x <= (playerBullet2X + 50) && (e4x + 75) >= playerBullet2X  && e4y <= (playerBullet2Y + 50) && (e4y + 100) >= playerBullet2Y)
  {
    e4x = Math.floor(Math.random() * 100) + 400;
    e4y = -250;
    playerBullet2X = -50;
    bullet2shot = false;
    bullet2wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e4x <= (playerBullet3X + 50) && (e4x + 75) >= playerBullet3X  && e4y <= (playerBullet3Y + 50) && (e4y + 100) >= playerBullet3Y)
  {
    e4x = Math.floor(Math.random() * 100) + 400;
    e4y = -250;
    playerBullet3X = -50;
    bullet3shot = false;
    bullet3wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }

  if (e4y >= 1500)
  {
    e4x = Math.floor(Math.random() * 100) + 400;
    e4y = -250;
  }
}
function enemy5Move()
{
  e5y += enemySpeed;

  if (e5x <= (playerBullet1X + 50) && (e5x + 75) >= playerBullet1X  && e5y <= (playerBullet1Y + 50) && (e5y + 100) >= playerBullet1Y)
  {
    e5x = Math.floor(Math.random() * 100) + 700;
    e5y = -100;
    playerBullet1X = -50;
    bullet1shot = false;
    bullet1wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e5x <= (playerBullet2X + 50) && (e5x + 75) >= playerBullet2X  && e5y <= (playerBullet2Y + 50) && (e5y + 100) >= playerBullet2Y)
  {
    e5x = Math.floor(Math.random() * 100) + 700;
    e5y = -100;
    playerBullet2X = -50;
    bullet2shot = false;
    bullet2wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }
  if (e5x <= (playerBullet3X + 50) && (e5x + 75) >= playerBullet3X  && e5y <= (playerBullet3Y + 50) && (e5y + 100) >= playerBullet3Y)
  {
    e5x = Math.floor(Math.random() * 100) + 700;
    e5y = -100;
    playerBullet3X = -50;
    bullet3shot = false;
    bullet3wasShot = false;
    console.log("HIT");
    enemiesShot = enemiesShot + 1;
  }

  if (e5y >= 1500)
  {
    e5x = Math.floor(Math.random() * 100) + 700;
    e5y = -100;
  }
}
function enemy6Move()
{
  
}
function enemy7Move()
{
  
}
function enemy8Move()
{
  
}
function enemy9Move()
{
  
}
function enemy10Move()
{
  
}


//all enemy shooting functions
function enemy1shoots()
{
  setTimeout(() => {
    e1b1y += enemyBulletSpeed;
  }, 1000);
  setTimeout(() => {
    e1b2y += enemyBulletSpeed;
  }, 2000);
  setTimeout(() => {
    e1b3y += enemyBulletSpeed;
  }, 3000);

  //reset the postions once off screen
  if (e1b1y >= 1500)
  {
    setTimeout(() => {
      e1b1y = e1y;
      e1b1x = e1x + 25;
    }, 1000);
  }
  if (e1b2y >= 1500)
  { 
    setTimeout(() => {
      e1b2y = e1y;
      e1b2x = e1x + 25;
    }, 2000);
  }
  if (e1b3y >= 1500)
  { 
    setTimeout(() => {
      e1b3y = e1y;
      e1b3x = e1x + 25;
    }, 3000);
  }
}

function enemy2shoots()
{
  setTimeout(() => {
    e2b1y += enemyBulletSpeed;
  }, 1000);
  setTimeout(() => {
    e2b2y += enemyBulletSpeed;
  }, 2000);
  setTimeout(() => {
    e2b3y += enemyBulletSpeed;
  }, 3000);

  //reset the postions once off screen
  if (e2b1y >= 1500)
  {
    setTimeout(() => {
      e2b1y = e2y;
      e2b1x = e2x + 25;
    }, 1000);
  }
  if (e2b2y >= 1500)
  { 
    setTimeout(() => {
      e2b2y = e2y;
      e2b2x = e2x + 25;
    }, 2000);
  }
  if (e2b3y >= 1500)
  { 
    setTimeout(() => {
      e2b3y = e2y;
      e2b3x = e2x + 25;
    }, 3000);
  }
}
function enemy3shoots()
{
  setTimeout(() => {
    e3b1y += enemyBulletSpeed;
  }, 1000);
  setTimeout(() => {
    e3b2y += enemyBulletSpeed;
  }, 2000);
  setTimeout(() => {
    e3b3y += enemyBulletSpeed;
  }, 3000);

  //reset the postions once off screen
  if (e3b1y >= 1500)
  {
    setTimeout(() => {
      e3b1y = e3y;
      e3b1x = e3x + 25;
    }, 1000);
  }
  if (e3b2y >= 1500)
  { 
    setTimeout(() => {
      e3b2y = e3y;
      e3b2x = e3x + 25;
    }, 2000);
  }
  if (e3b3y >= 1500)
  { 
    setTimeout(() => {
      e3b3y = e3y;
      e3b3x = e3x + 25;
    }, 3000);
  }
}

function enemy4shoots()
{
  e4b1y += enemyBulletSpeed;
  e4b2y += enemyBulletSpeed;
  e4b2x -= 0.5;
  e4b3y += enemyBulletSpeed;
  e4b3x += 0.5;

  //reset the postions once off screen
  if (e4b1y >= 1500)
  {
    e4b1y = e4y +5;
    e4b1x = e4x + 25;
  }
  if (e4b2y >= 1500)
  { 
    e4b2y = e4y;
    e4b2x = e4x;
  }
  if (e4b3y >= 1500)
  { 
    e4b3y = e4y;
    e4b3x = e4x + 50;
  }
}

function enemy5shoots()
{
  e5b1y += enemyBulletSpeed;
  e5b2y += enemyBulletSpeed;
  e5b2x -= 0.5;
  e5b3y += enemyBulletSpeed;
  e5b3x += 0.5;

  //reset the postions once off screen
  if (e5b1y >= 1500)
  {
    e5b1y = e5y +5;
    e5b1x = e5x + 25;
  }
  if (e5b2y >= 1500)
  { 
    e5b2y = e5y;
    e5b2x = e5x;
  }
  if (e5b3y >= 1500)
  { 
    e5b3y = e5y;
    e5b3x = e5x + 50;
  }
}

