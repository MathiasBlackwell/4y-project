console.log("Main.js script loaded")
////doesnt allow for arrow keys and space bar to move the screen
window.addEventListener("keydown", function(e) {
    if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
        e.preventDefault();
    }
}, false);
//get canvas and set it up uwu
const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");


let cutsceneAudio = new Audio('../public/assets/media/audio/cutscenemusic.m4a');
let gameAudio = new Audio('../public/assets/media/audio/gamemusic.m4a');
let bulletAudio1 = new Audio('../public/assets/media/audio/bulletShoot.m4a');
let bulletAudio2 = new Audio('../public/assets/media/audio/bulletShoot.m4a');
let bulletAudio3 = new Audio('../public/assets/media/audio/bulletShoot.m4a');


//==========================Variables===========================/
let scene = 0;
let enemiesShot = 0;
let enemiesNeeded = 10;

//this allows for bg scrolling
let bgY = 0;
let bgY2 = -1500;

//all player positions, initial and movement.
let playerHealth = 0; // 0 is 10 hearts, 9 is dead.
let playerDirection = 0;
let enemyDefeated = 0;
let enemyMustDefeat = 10;
let xposition = canvas.width/2;
let yposition = canvas.height/4*3;
let vx = 0;
let vy = 0;

//player shooting
let playerBullet1X = xposition;
let playerBullet1Y = yposition;
let playerBullet2X = 1550;
let playerBullet2Y = 1550; 
let playerBullet3X = 1550;
let playerBullet3Y = 1550;   
let pbulletSpeed = 5;

let shootPressed1 = false;
let bullet1shot = false;
let bullet1canShoot = true;
let bullet1wasShot = false;

let shootPressed2 = false;
let bullet2shot = false;
let bullet2canShoot = true;
let bullet2wasShot = false;

let shootPressed3 = false;
let bullet3shot = false;
let bullet3canShoot = true;
let bullet3wasShot = false;

//enemy variables
let e1x = Math.floor(Math.random() * 200) + 600;
let e1y = -150;
let e2x = Math.floor(Math.random() * 200) + 400;
let e2y = -50;
let e3x = Math.floor(Math.random() * 200) + 100;
let e3y = -300;
let e4x = Math.floor(Math.random() * 300) + 400;
let e4y = -250;
let e5x = Math.floor(Math.random() * 100) + 700;
let e5y = -100;

let enemySpeed = 1;
let enemyBulletSpeed = 2;

//enemy bullets they shoot 5 each, i couldve made this a class or array, however it is too late now to fix that.
let e1b1x = 0;
let e1b1y = 5000;
let e1b2x = 0;
let e1b2y = 1500;
let e1b3x = 0;
let e1b3y = 1500;

let e2b1x = 0;
let e2b1y = 5000;
let e2b2x = 0;
let e2b2y = 1500;
let e2b3x = 0;
let e2b3y = 1500;

let e3b1x = 0;
let e3b1y = 5000;
let e3b2x = 0;
let e3b2y = 1500;
let e3b3x = 0;
let e3b3y = 1500;

let e4b1x = 0;
let e4b1y = 5000;
let e4b2x = 0;
let e4b2y = 1500;
let e4b3x = 0;
let e4b3y = 1500;

let e5b1x = 0;
let e5b1y = 5000;
let e5b2x = 0;
let e5b2y = 1500;
let e5b3x = 0;
let e5b3y = 1500;


//declare images
let introIMG = new Image();
let bgIMG = new Image();
let gameoverIMG = new Image();
let bannerUIIMG = new Image();
let endIMG = new Image();

let playerIMG = new Image();
let playerBulletIMG = new Image();
let playerHealthIMG = new Image();

let enemyIMG = new Image();
let bulletIMG = new Image();

//link images
introIMG.src = "assets/media/scenes/intro.png";
bgIMG.src = "assets/media/bgs/bg1.png";
gameoverIMG.src = "assets/media/scenes/gameover.png";
bannerUIIMG.src = "assets/media/scenes/bannerUI.png";
endIMG.src = "assets/media/scenes/endscreen.png";

playerIMG.src = "assets/media/player/player_sprite.png";
playerBulletIMG.src = "assets/media/player/bullet_sprite.png";
playerHealthIMG.src = "assets/media/player/playerHealth.png";

enemyIMG.src = "assets/media/enemies/enemy1_sprite.png";
bulletIMG.src = "assets/media/enemies/bullet1_sprite.png";


//for intro coords
introx = 0;
introy = 0;

//each character with different animation speed needs their own animations
//however ill used the player animation speed for npcs too
//animation variables
let playerframes = 4;
let enemyframes = 4;
let introframes = 19;

//each indivdual animation needs its own current frame
let playercurrentFrame = 0; //used for animations
let introcurrentFrame = 0;
let enemycurrentFrame = 0;

//time
let playerinitial = new Date().getTime();
let enemyinitial = new Date().getTime();
let introinitial = new Date().getTime();
let playercurrent; 
let introcurrent;
let enemycurrent;



//==========================Main Game Functions===========================/

function update()
{
    UpdateScenes();
    bgScroll();

    playerMVMT();
    playerBounds();
    playerHitbox();

    shootingBullets();
    PlayerAnimation();
    EnemyAnimation();
    
    if (scene == 1)
    {
        enemy1Move();
        enemy1shoots();
        enemy2Move();
        enemy2shoots();
        enemy3Move();
        enemy3shoots();
        enemiesNeeded = 10;
    }
    if (scene == 2)
    {
        enemy1Move();
        enemy1shoots();
        enemy2Move();
        enemy2shoots();
        enemy3Move();
        enemy3shoots();
        enemy4Move();
        enemy4shoots();
        enemy5Move();
        enemy5shoots();
        enemiesNeeded = 25;
    }
    if (scene == 3)
    {
        enemy1Move();
        enemy1shoots();
        enemy2Move();
        enemy2shoots();
        enemy3Move();
        enemy3shoots();
        enemy4Move();
        enemy4shoots();
        enemy5Move();
        enemy5shoots();
        enemiesNeeded = 50;
    }

    if (playerHealth == 10)
    {
        scene = 100;
    }


}

function draw()
{
    
    context.clearRect(0, 0, canvas.width, canvas.height); //always clear the canvas

    context.drawImage(bgIMG, 0, bgY, 1000, 1500);
    context.drawImage(bgIMG, 0, bgY2, 1000, 1500);

    //player sprite
    context.drawImage(playerIMG, (75 * playercurrentFrame), (playerDirection * 100), 75, 100, xposition, yposition, 75, 100);

    if (bullet1shot)
    {
        context.drawImage(playerBulletIMG, playerBullet1X, playerBullet1Y, 50, 50);
    }
    if (bullet2shot)
    {
        context.drawImage(playerBulletIMG, playerBullet2X, playerBullet2Y, 50, 50);
    }
    if (bullet3shot)
    {
        context.drawImage(playerBulletIMG, playerBullet3X, playerBullet3Y, 50, 50);
    }


    context.drawImage(bulletIMG, e1b1x, e1b1y, 22.5, 75);
    context.drawImage(bulletIMG, e1b2x, e1b2y, 22.5, 75);
    context.drawImage(bulletIMG, e1b3x, e1b3y, 22.5, 75);
    context.drawImage(enemyIMG, (75 * enemycurrentFrame), 0, 75, 100, e1x, e1y, 75, 100);
    context.drawImage(bulletIMG, e2b1x, e2b1y, 22.5, 75);
    context.drawImage(bulletIMG, e2b2x, e2b2y, 22.5, 75);
    context.drawImage(bulletIMG, e2b3x, e2b3y, 22.5, 75);
    context.drawImage(enemyIMG, (75 * enemycurrentFrame), 0, 75, 100, e2x, e2y, 75, 100);
    context.drawImage(bulletIMG, e3b1x, e3b1y, 22.5, 75);
    context.drawImage(bulletIMG, e3b2x, e3b2y, 22.5, 75);
    context.drawImage(bulletIMG, e3b3x, e3b3y, 22.5, 75);
    context.drawImage(enemyIMG, (75 * enemycurrentFrame), 0, 75, 100,  e3x, e3y, 75, 100);
    context.drawImage(bulletIMG, e4b1x, e4b1y, 22.5, 75);
    context.drawImage(bulletIMG, e4b2x, e4b2y, 22.5, 75);
    context.drawImage(bulletIMG, e4b3x, e4b3y, 22.5, 75);
    context.drawImage(enemyIMG, (75 * enemycurrentFrame), 0, 75, 100,  e4x, e4y, 75, 100);
    context.drawImage(bulletIMG, e5b1x, e5b1y, 22.5, 75);
    context.drawImage(bulletIMG, e5b2x, e5b2y, 22.5, 75);
    context.drawImage(bulletIMG, e5b3x, e5b3y, 22.5, 75);
    context.drawImage(enemyIMG, (75 * enemycurrentFrame), 0, 75, 100,  e5x, e5y, 75, 100);

    //ui elements are drawn last to be on top:
    context.drawImage(bannerUIIMG,0,1300);
    context.font = "48px serif";
    context.fillStyle = "#ffffff";
    let gameScore = "Enemies Killed: " + enemiesShot + " / " + enemiesNeeded;
    context.fillText(gameScore, 10, 1370);
    context.fillText("Level: "+scene, 800,1370);
    context.drawImage(playerHealthIMG, 0, (playerHealth * 101), 1000, 101, 0, 1400, 1000, 101);

    if (scene == 100)
    {
        context.drawImage(gameoverIMG, 0, 0);

    }

    if (scene == 200)
    {
        context.drawImage(endIMG, 0, 0);

    }
    
}

function drawIntro()
{
    context.drawImage(introIMG, (1000 * introcurrentFrame), 0, 1000, 1500, introx, introy, 1000,1500);
}

function gameLoop()
{

    IntroAnimation();
    if (scene == 0)
    {
        drawIntro();
        cutsceneAudio.play();
        if (introcurrentFrame == (introframes-1))
        {
            scene = 1;
        }
    }


    if (scene >= 1)
    {
        gameAudio.play();
        draw();
        update();
    }

    requestAnimationFrame(gameLoop);
}

function IntroAnimation()
{
    introcurrent = new Date().getTime();
    if (introcurrent - introinitial >= 2000) {
        introcurrentFrame = (introcurrentFrame + 1) % introframes;
        introinitial = introcurrent;
    }

}

function PlayerAnimation()
{
    //player animation
    playercurrent = new Date().getTime();
    if (playercurrent - playerinitial >= 250) {
        playercurrentFrame = (playercurrentFrame + 1) % playerframes;
        playerinitial = playercurrent;
    }
}

function EnemyAnimation()
{
    //enemy animation
    enemycurrent = new Date().getTime();
    if (enemycurrent - enemyinitial >= 250) {
        enemycurrentFrame = (enemycurrentFrame + 1) % enemyframes;
        enemyinitial = enemycurrent;
    }
}


//==========================Other Game Functions===========================/

function bgScroll()
{
    bgY += 0.5;
    if (bgY >= 1500)
    {
        bgY = 0;
    }
    bgY2 += 0.5;
    if (bgY2 >= 0)
    {
        bgY2 = -1500;
    }
}


//==========================Event Listeners===========================/

window.requestAnimationFrame(gameLoop);
window.addEventListener('keydown',function(e)
{
    if (e.code == "KeyD") 
    {
        vx = 5;
        playerDirection = 2;
    };
    if (e.code == "KeyS")
    {
        vy = 5;
        playerDirection = 0;
    };
    if (e.code == "KeyA")
    {
        vx = -5;
        playerDirection = 1;
    };
    if (e.code == "KeyW")
    {
        vy = -5;
        playerDirection = 0;
    };

    if (e.code == "ArrowRight") 
    {
        vx = 5;
        playerDirection = 2;
    };
    if (e.code == "ArrowDown")
    {
        vy = 5;
        playerDirection = 0;
    };
    if (e.code == "ArrowLeft")
    {
        vx = -5;
        playerDirection = 1;
    };
    if (e.code == "ArrowUp")
    {
        vy = -5;
        playerDirection = 0;
    };

    if (e.code == "Space") 
    {
        if (shootPressed1 == false)
        {
            shootPressed1 = true;
        }

        if (bullet1wasShot == true && shootPressed2 == false)
        {
            shootPressed2 = true;
        }

        if (bullet1wasShot == true && bullet2wasShot == true && shootPressed3 == false)
        {
            shootPressed3 = true;
        }

    };

});

window.addEventListener('keyup',function(e)
{
    if (e.code == "KeyD") vx = 0;
    if (e.code == "KeyS") vy = 0;
    if (e.code == "KeyA") vx = 0;
    if (e.code == "KeyW") vy = 0;

    if (e.code == "ArrowRight") vx = 0;
    if (e.code == "ArrowDown") vy = 0;
    if (e.code == "ArrowLeft") vx = 0;
    if (e.code == "ArrowUp") vy = 0;


    //this is for debug process only, will be deleted in the final game.
    if (e.code == "Digit1") scene = 1;
    if (e.code == "Digit2") scene = 2;
    if (e.code == "Digit3") scene = 3;
    if (e.code == "Digit4") scene = 4;
    if (e.code == "Digit5") scene = 5;
    if (e.code == "Digit6") scene = 6;
    if (e.code == "Digit7") scene = 7;
    if (e.code == "Digit8") scene = 8;
    if (e.code == "Digit9") scene = 9;
    if (e.code == "Digit0") scene = 10;
    
});

