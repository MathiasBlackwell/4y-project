//All scenes functions, handlings scene changes, updating sprites, etc.
console.log("Scenes Loaded");

function UpdateScenes()
{
    switch(scene) 
    {
        //1-10 is game levels, always call back to them, easier to keep em as this.
        case 1:
            bgIMG.src = "assets/media/bgs/bg1.png";
            enemyIMG.src = "assets/media/enemies/enemy1_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet1_sprite.png";
            enemySpeed = 1;
            enemyBulletSpeed = 2;
            if (enemiesShot >= enemiesNeeded)
            {
              scene = 2;
              enemiesShot = 0;
            };
          break;
        case 2:
            bgIMG.src = "assets/media/bgs/bg2.png";
            enemyIMG.src = "assets/media/enemies/enemy2_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet2_sprite.png";
            enemySpeed = 1.5;
            enemyBulletSpeed = 3;
            if (enemiesShot >= enemiesNeeded)
            {
              scene = 3;
              enemiesShot = 0;
            };
          break;
        case 3:
            bgIMG.src = "assets/media/bgs/bg3.png";
            enemyIMG.src = "assets/media/enemies/enemy3_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet3_sprite.png";
            enemySpeed = 2;
            enemyBulletSpeed = 4;
            if (enemiesShot >= enemiesNeeded)
            {
              scene = 200;
            }
          break;
        case 4:
            bgIMG.src = "assets/media/bgs/bg4.png";
            enemyIMG.src = "assets/media/enemies/enemy4_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet4_sprite.png";
            enemymoveright = 4;
            enemymoveleft = -4;
            ebulletSpeed = 4;
          break;
        case 5:
            bgIMG.src = "assets/media/bgs/bg5.png";
            enemyIMG.src = "assets/media/enemies/enemy5_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet5_sprite.png";
            enemymoveright = 5;
            enemymoveleft = -5;
            ebulletSpeed = 5;
          break;
        case 6:
            bgIMG.src = "assets/media/bgs/bg6.png";
            enemyIMG.src = "assets/media/enemies/enemy6_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet6_sprite.png";
            enemymoveright = 6;
            enemymoveleft = -6;
            ebulletSpeed = 6;
          break;
        case 7:
            bgIMG.src = "assets/media/bgs/bg7.png";
            enemyIMG.src = "assets/media/enemies/enemy7_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet7_sprite.png";
            enemymoveright = 7;
            enemymoveleft = -7;
            ebulletSpeed = 7;
          break;
        case 8:
            bgIMG.src = "assets/media/bgs/bg8.png";
            enemyIMG.src = "assets/media/enemies/enemy8_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet8_sprite.png";
            enemymoveright = 8;
            enemymoveleft = -8;
            ebulletSpeed = 8;
          break;
        case 9:
            bgIMG.src = "assets/media/bgs/bg9.png";
            enemyIMG.src = "assets/media/enemies/enemy9_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet9_sprite.png";
            enemymoveright = 9;
            enemymoveleft = -9;
            ebulletSpeed = 9;
          break;
        case 10:
            bgIMG.src = "assets/media/bgs/bg10.png";
            enemyIMG.src = "assets/media/enemies/enemy10_sprite.png";
            bulletIMG.src = "assets/media/enemies/bullet10_sprite.png";
            enemymoveright = 10;
            enemymoveleft = -10;
            ebulletSpeed = 10;
          break;
        //additional scenes as transitions between scenes, like player going through portal to next level etc.
        //im thinking i do 12 as 1 -> 2, 23 as 2 -> 3, 67 6 -> 7, etc. easier to later remember.
    }
}